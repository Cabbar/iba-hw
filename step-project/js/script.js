let servicesTabs = document.querySelectorAll(".tab-item");
let servicesContent = document.querySelectorAll(".our-services-content")
for (let i = 0; i<servicesTabs.length;i++){

    servicesTabs[i].addEventListener("click",function (event) {
        for (let j =0;j<servicesTabs.length;j++){
            servicesTabs[j].classList.remove("services-active");
            servicesContent[j].classList.add("hidden");

        }
        servicesContent[i].classList.remove("hidden");

        event.currentTarget.classList.add("services-active");



    });

}

let amazingWorkBtn = document.querySelector(".load-more-btn");

let unloadedImg = document.querySelectorAll(".amazing-work-img-item-2")
amazingWorkBtn.addEventListener("click",function (loadMore) {
   for (let i =0;i<unloadedImg.length;i++){
       unloadedImg[i].classList.remove("hidden");

   }


   amazingWorkBtn.hidden = true;
});


let amazingWorkTabs = document.querySelectorAll(".amazing-work-item")

let amazingWorkContent = document.querySelectorAll(".amazing-work-img-main")
for ( let count = 0;count<amazingWorkTabs.length;count++){
    amazingWorkTabs[count].addEventListener("click",function (amazingWorkEvent) {

        for (let a =0;a<amazingWorkTabs.length;a++){
            amazingWorkTabs[a].classList.remove("amazing-work-active");
            amazingWorkContent[a].classList.add("hidden");

        }
        amazingWorkContent[count].classList.remove("hidden");

        amazingWorkEvent.currentTarget.classList.add("amazing-work-active");
        amazingWorkBtn.hidden = true;
        for(let b = 0;b<unloadedImg.length;b++){
            unloadedImg[b].classList.add("hidden")
        }

    })
}
amazingWorkTabs[0].addEventListener("click",function (showLoadMore) {
amazingWorkBtn.hidden = false;
});

