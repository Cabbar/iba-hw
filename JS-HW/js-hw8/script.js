let script = document.querySelector("script");
let priceInput = document.getElementById("price");
let body = document.querySelector("body");
let warning = document.querySelector(".warning");

priceInput.addEventListener("focus",function (focus) {
    priceInput.style.border = "2px solid green";
let removeWarning = document.querySelector(".empty-warning");

    warning.innerText = ""
    });
priceInput.addEventListener("focus", function (focusOther) {
    let nanWarning = document.querySelector(".nan-warning");

    warning.innerText = "";
});
priceInput.addEventListener("blur",function (unfocus) {
    if (priceInput.value.length===0){
        priceInput.style.borderColor = "red";
        warning.innerText = "Field is empty";
    }
   else if (isNaN(priceInput.value)){
        priceInput.style.borderColor = "red";
        warning.innerText = "Value is not a number"
    }
   else if (priceInput.value<0){
        priceInput.style.borderColor = "red";
        warning.innerText = "Value is negative"
    }
   else {
        priceInput.style.borderColor = "gray";
        let addedPrice = document.createElement("div");
        addedPrice.classList.add("added-price");
        body.append(addedPrice);
        let spanTxt = document.createElement("span");
        spanTxt.innerText = `Current Price is : ${priceInput.value}`;
let removeBtn = document.createElement("button");
removeBtn.innerText = "X";
removeBtn.addEventListener("click", function (removePrice) {
    let prices = document.querySelectorAll(".added-price");
    removePrice.target.parentElement.remove();

});
        let label = document.querySelector("label");
        label.before(spanTxt);
        addedPrice.appendChild(spanTxt);
        addedPrice.appendChild(removeBtn);
        script.before(addedPrice);
    }
    priceInput.value = '';
});

