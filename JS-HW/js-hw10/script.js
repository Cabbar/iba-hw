let eye = document.getElementsByClassName("icon-password");
let passFields = document.querySelectorAll(".pass-field");
for (let i = 0;i<eye.length;i++){

    eye[i].addEventListener("click",function (event) {
        if (event.target.classList.contains("fa-eye")){
            event.target.classList.replace("fa-eye","fa-eye-slash");
            passFields[i].type = "text";

        }
        else {
            event.target.classList.replace("fa-eye-slash","fa-eye");
            passFields[i].type = "password"
        }
    })
}
let warning = document.createElement("span");

let btn = document.querySelector(".btn");
btn.addEventListener("click",function (clickEvent) {
    if (passFields[0].value===passFields[1].value){
        window.location.reload()
    }
    else {
        warning.innerText = "PASSWORD DON'T MATCH";
        warning.style.color = "red";
        document.body.append(warning);
    }
    let script = document.querySelector("script");
    script.before(warning);
});

